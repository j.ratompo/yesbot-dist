# Cover Page

** Last Updated: ** 25/03/2024


# Cloud Service Agreement


### Introduction

Thanks for using Yesbot !

These terms of service ("Terms") apply to Customer access and use of Yesbot platform and tools (the "Service") provided by us as the Service Provider. Please read them carefully.

If you as Customer access or use the Service, and have signed this cover page, it means you agree to be bound by all of the terms below. So, before you use the Service and sign this cover page, please read all of the terms. If you don't agree to all of the terms below, please do not use the Service nor sign this cover page. Also, if you disagree with a term, please let us know by e-mailing **info@alqemia.com**.


### Glossary

«Yesbot platform», or «Yesbot TAAS platform» encompasses all the components of the Yesbot Cloud software solution, including, but not limited to «Yesbot Portal» and the Testbot tests analysis engine «Testbot engine».

«Provider» designates the entity owning the explotation rights and intellectual ownership of the Yesbot Platform providing the service, ie. Alqemia Studio

«Customer» designate any entity paying or not, wanting to be provided the service of the Yesbot Platform

«Users» are designated entities who are authorized by Customer and Provider, to use Customer's account to use the Yesbot Platform. Users must respect all the terms of the current agreement to the same extent of Customer.


### Summary

This Agreement has 3 parts: (1) the Order Form and (2) the Key Terms, both of which are on this Cover Page, and (3) the Common Paper Cloud Service Standard Terms Version 1.1 posted at [[commonpaper.com/standards/cloud-service-agreement/1.1]{.underline}](https://commonpaper.com/standards/cloud-service-agreement/1.1) Licensed under the CC-BY 4.0

YOU MUST READ THE AFOREMENTIONED Common Paper Cloud Service Standard Terms BEFORE PROCEEDING

The artifact made by these 3 partsconstitute the entire agreement between Customer andProvider regarding the use of the Service, superseding any prior agreements between Customer and Provider relating to your use of the Service.


**("Standard Terms")**, which is incorporated by reference. If there is any inconsistency between the parts of the Agreement, the part listed earlier will control over the part listed later for that inconsistency. Capitalized and highlighted words have the meanings given on the Cover Page. However, if the Cover Page omits or does not define a highlighted word, the default meaning will be "none" or "not applicable" and the correlating clause, sentence, or section does not apply to this Agreement. All other capitalized words have the meanings given in the Standard Terms.

----
>Order Form

The key business terms of this Agreement are as follows:

### Cloud Service

#### The Cloud Service is:

Yesbot Platform is Test As A Service platform, providing tools to generate, maintain and manage automated tests for any client application. Yesbot consists in a client UI, and several server components. Yesbot main focus is API backend automated testing.


### Disclaimers

Yesbot is a tool to generate, maintain, manage and run tests on your application. The Provider cannot be held responsible for the misusage and/or misunderstanding of this tool functioning, capabilities and architecture that would result in problems affecting your application development times, bugs, etc. We will give our best efforts to help you using this tool so that in the ends it benefits the Customer.

FOR ALPHA VERSIONS ONLY : As Yesbot current released version is an alpha, the Yesbot Platform is under active development. This may result in (not limited to) bugs, unplanned interruptions of service, instabilities and unexpected behavior. Customer must be aware of this and take it into account when using the Yesbot Platform. At this stage also Customer will seek understanding, cooperation and feedback from the Customer regarding the service usage.


### Changes to these Terms

We as Provider reserve the right to modify these Terms at any time. For instance, we may need to change these Terms if we come out with a new feature or for some other reason.

Whenever we make changes to these Terms, the changes are effective  immediately after we post such revised Terms (indicated by revising the date at the top of these Terms).

However it's our responsibility to send you a notification email to you as Customer of these changes at most 15 days after the changes are made public. From this date, you have 15 days to signify you reject those changes by email or certified letter. Then we'll discuss on ways to terminate the current agreement and sign a new agreement with you, or simply proceed with the current agreement termination with no agreement replacement.

Past this delay, if we do not receive a refusal message from you, we'll consider you accept the new terms, and the previous agreement ends, to be automatically replaced with the new agreement including these changes.



### Yesbot Platform version specificities


The current released version for the Yesbot Platform is : ** 1.0.0_alpha **

The available features and pricing of parts or the whole service described in this document may be subjected to specificities according to the currently provided version. In that case it will be explicitly written near the affected clause/paragraph/sentence.

Provider will make their best efforts to hold good communications with Customer regarding new Yesbpt Platform releases. However it is the responsibility of Customer to keep being up-to-date regarding the downloadable versioned artifacts related to Yesbot distributed by the Provider, and integrating them into their software environments.


### Offering details

In order to ensure fair use among all the users of the Yesbot Platform, and prevent one or a few users to use all the available cloud resources to the detriment of others, The Yesbot Platform has a quotas mechanism in order to monitor an limit resource usage.

Therefore the Yesbot Service pricing offering consists in two component :
- Tests units and applications number amounts
- Subscription

In order to have access to the full Yesbot features, Customer must have an active subscription, and more or the same as applications number and tests units amounts than they're using. The number of available tests is applicable across all applications, it's not an amount for each application.


### Subscription Start Date

#### The date access to the Cloud Service starts:

\[ *Defined each time an agreement is sent* \]


## Subscription Period

### Length of Cloud Service access

FOR ALPHA VERSIONS ONLY : The period is 6 months, with 1 maximum possible renewal. Then the beta and above versions periods apply.
FOR BETA VERSIONS AND ABOVE : 1 year(s) minimum and by default, or 2 years / 5 years / 10 years. Price and discounts apply regarding the chosen period.

### Cloud Service Fees

\[ *Defined each time an agreement is sent* \]

Modifying Section 5.1, fees are inclusive of taxes.

FOR ALPHA VERSIONS ONLY : No fee apply
FOR BETA VERSIONS AND ABOVE : N/A

## Payment Period

### Time frame for Customer to pay invoices

30 day(s) from **Customer\'s** receipt of invoice

## Invoice Period

**How frequently Provider sends invoices : ** **Provider** will send invoices Annually

### Auto-renewal

Modifying Section 6.1 of the Standard Terms, the **Subscription Period** does not automatically renew and will expire at the end of the **Subscription Period**.

### Use Limitations

FOR THE ALPHA VERSIONS ONLY :\
- Each user : 1 applications maximum\
- For all the applications of an user : 50 tests units maximum

FOR BETA VERSIONS AND ABOVE :
- N/A
- N/A

### Technical Support

Questions or comments about the Service may be directed to us at the support email address

Support email : info@alqemia.com\
Support includes :\
- Assistance & Troubleshooting with integrating and using the platform
(limited to 2h/week per customer)\
- New features suggestions ; Technical, ergonomy and other general
feedback gathering

### Feedback, Questions and Comments

Please let us know what you think of the Service, these Terms and, in general, the Yesbot Platform. When you provide us with any feedback, comments, questions or suggestions about the Service, these Terms and, in general, Yesbot, you irrevocably assign to us all of your right, title and interest in and to your feedback, comments and suggestions.


----
>Key Terms



The key legal terms of this Agreement are as follows:­­

## Effective Date

### The date the Agreement starts

Date of last signature on this Cover Page

### Governing Law

The validity of these Terms and the rights, obligations, and relations of the parties under these Terms will be construed and determined under and in accordance with the laws of the laws of Quebec, Canada, without regard to conflicts of law principles.


## Chosen Courts

### Jurisdiction

You expressly agree that exclusive jurisdiction for any dispute with the Service or relating to your use of it, resides in the courts of (whether state, federal, or otherwise) located in Quebec, Canada and you further agree and expressly consent to the exercise of personal jurisdiction in the courts of the Quebec, Canada located in Montreal, Quebec, Canada in connection with any such dispute including any claim involving Service. You further agree that you and Service will not commence against the other a class action, class arbitration or other representative action or proceeding.

### Indemnity

THE SERVICE AND ANY OTHER SERVICE AND CONTENT INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THE SERVICE ARE PROVIDED TO YOU (AS CUSTOMER) ON AN AS IS OR AS AVAILABLE BASIS WITHOUT ANY REPRESENTATIONS OR WARRANTIES OF ANY KIND. WE DISCLAIM ANY AND ALL WARRANTIES AND REPRESENTATIONS (EXPRESS OR IMPLIED, ORAL OR WRITTEN) WITH RESPECT TO THE SERVICE AND CONTENT INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THE SERVICE WHETHER ALLEGED TO ARISE BY OPERATION OF LAW, BY REASON OF CUSTOM OR USAGE IN THE TRADE, BY COURSE OF DEALING OR OTHERWISE.

IN NO EVENT WILL PROVIDER BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY SPECIAL, INDIRECT, INCIDENTAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES OF ANY KIND ARISING OUT OF OR IN CONNECTION WITH THE SERVICE OR ANY OTHER SERVICE AND/OR CONTENT INCLUDED ON OR OTHERWISE MADE AVAILABLE TO YOU THROUGH THE SERVICE, REGARDLESS OF THE FORM OF ACTION, WHETHER IN CONTRACT, TORT, STRICT LIABILITY OR OTHERWISE, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES OR ARE AWARE OF THE POSSIBILITY OF SUCH DAMAGES. OUR TOTAL LIABILITY FOR ALL CAUSES OF ACTION AND UNDER ALL THEORIES OF LIABILITY WILL BE LIMITED TO THE AMOUNT YOU PAID TO PROVIDER. THIS SECTION WILL BE GIVEN FULL EFFECT EVEN IF ANY REMEDY SPECIFIED IN THIS AGREEMENT IS DEEMED TO HAVE FAILED OF ITS ESSENTIAL PURPOSE.

You agree to defend, indemnify and hold us harmless from and against any and all costs, damages, liabilities, and expenses (including attorneys' fees, costs, penalties, interest and disbursements) we incur in relation to, arising from, or for the purpose of avoiding, any claim or demand from a third party relating to your use of the Service or the use of the Service by any person using your account, including any claim that your use of the Service violates any applicable law or regulation, or the rights of any third party, and/or your violation of these Terms.

However this indemnity section allow some limitations, described in the following section :


## Covered Claims


### Claims covered by indemnity obligations

#### Customer Covered Claims:

A lawsuit about the content the customer upload to our product violating someone else's intellectual property rights\
A lawsuit about about the customer breaching the restrictions of using our product, such as using it for an illegal purpose

### General Cap Amount

#### Limitation of liability amount for most claims

The greater of \$5,000.00 or 1.0 times the fees paid or payable by **Customer** to **Provider** in the 12 month period immediately before the claim

### Increased Claims

#### Specific claims covered by the Increased Cap Amount

- An Indemnifying Party's indemnification obligation
- Breach of Section 4 (Privacy & Security) resulting from gross negligence or willful misconduct
- Breach of Section 12 (Confidentiality) resulting from gross negligence or willful misconduct (however, excluding any data or security breaches)
- Breach of Section 4 (Privacy and Security)
- Breach of Section 12 (Confidentiality)
- Reselling Yesbot access and credentials to an unauthorized third-party

### Unlimited Claims

#### Claims excluded from any limitation of liability

- An Indemnifying Party\'s indemnification obligation
- Breach of Section 4 (Privacy & Security) resulting from gross negligence or willful misconduct
- Breach of Section 12 (Confidentiality) resulting from gross negligence or willful misconduct (however, excluding any data or security breaches)
- Breach of Section 4 (Privacy & Security)
- Breach of Section 12 (Confidentiality) (however, excluding any data or security breaches)

Unauthorized retro-engineering on closed-source components, and sharing Yesbot intellectual property with unauthorized third-party. Exploiting Yesbot vulnerabilities to gain any commercial advantage regarding other Yesbot clients or on the entity currently owning Yesbot.


## Attachments and Supplements

### Third-Party Services

Occasionally, we may use of third party websites or services that we do not own or control. Your use of the Service may also imply the use of applications that are developed or owned by a third party. Your use of such third party applications, websites, and services is governed by that party's own terms of service or privacy policies. We encourage you to read the terms and conditions and privacy policy of any third party application, website or service that we tell you in our official documentation that our Service use.

### Creating Accounts

When you as the Customer create an account or use another service to log in to the Service, you agree to maintain the security of your password and accept all risks of unauthorized access to any data or other information you provide to the Service.

If you discover or suspect any Service security breaches, please let us as the provider, know as soon as possible.

### Customer Content & Conduct

You as the Customer are responsible for the data you put on the Yesbot Platform.

You can remove the content that you posted by deleting it. Once you delete your content, it will not appear on the Service, but copies of your deleted content may remain in our system or backups for some period of time. We will retain web server access logs for a maximum of 2 years and then delete them.


### Acceptable Use Policy

-   Authorized Use: The platform is intended solely for the purpose of creating, managing, and running automated tests for API backends. Users must refrain from using the platform for any unlawful or unauthorized purposes.
-   Compliance with Terms of Service: Users are required to comply with all terms outlined in the platform\'s Terms of Service agreement.Any violations may result in the suspension or termination of the user\'s account.
-   Responsible Usage: Users are responsible for the content they generate and share on the platform. Content must not contain any 
malicious code, viruses, or harmful elements that could compromise the platform\'s integrity or the security of other users\'  systems.
-   Intellectual Property: Users must respect intellectual property rights, including copyrights and trademarks, when using the platform. Do not use the platform to infringe upon the intellectual property rights of others.
-   Data Privacy: Users must adhere to all applicable data privacy laws and regulations when using the platform. Do not access or use any data without proper authorization, and take necessary precautions to protect sensitive information.
-   Prohibited Activities: The following activities are strictly prohibited on the platform:\
    - Attempting to gain unauthorized access to other users\' accounts or data.\
    - Collect any personal information about other users
    - Engaging in any form of hacking, phishing, or other malicious activities.\
    - Use the Service in any manner that could interfere with, disrupt, negatively affect or inhibit other users from fully enjoying the Service or that could damage, disable, overburden or impair the functioning of the Service
    - Trying to circumvent authorization, security, payment, and quota restrictions mechanisms of the platform. Circumvent or attempt to circumvent any filtering, security measures, rate limits or other features designed to protect the Service, users of the Service, or third parties.\
    - Violating the privacy of other users or third parties. Users are encouraged to report any violations of this acceptable use policy or suspicious activities to the platform administrators promptly.
-	Prohibited data : Since Yesbot Platform provides Customer a way to communicate and/or host Customer data (according to the applicable business deal), Customer has the responsability to ensure, to the best of their knowledge and abilities that the provided data does not correspond to any of these descriptions :
		- Content that is illegal or unlawful, that would otherwise create liability
		- Content that may infringe or violate any patent, trademark, trade secret, copyright, right of privacy, right of publicity or other intellectual or other right of any party
		- Viruses, corrupted data or other harmful, disruptive or destructive files or code.
-   Consequences of Violation: Violation of this acceptable use policy may result in the suspension or termination of the user\'s account, as well as legal action if warranted.

By using Yesbot, you agree to abide by this acceptable use policy. We reserve the right to update or modify this policy at any time. Continued use of the platform constitutes acceptance of any revisions.


### Yesbot Platform and Provider Intellectual Property

We as the Provider put a lot of effort into creating the Service including, the logo and all designs, text, graphics, pictures, code, configuration files, documentation,  information and other content (excluding your content). This property is owned by us or our licensors and it is protected by Canada, Quebec and international copyright laws. We grant you limited rights to use it, described as follow :

- We grant rights to use our distributed client code, licensed under the HGPL license, in order to ensure the proper working of some of the advanced yesbot Platform features.
- We grant you the right to use and display the Yesbot logo specifically to indicate that your product, app, service or website use Yesbot Platform in order to perform what the Service goal is. The mention must be explicit, for instance «tests performed by Yesbot» or any equivalent phrasing.
- We grant you the right to create a hyperlink to the Service.
- We grant you the right to use and display the Provider logo in all your communication and public relations artifacts, specifically to indicate that you have currently an on going business relationship with Provider, via the Service or any other way that is also known and approved by Provider.
- We do not grant you the right to use, frame or utilize framing techniques to enclose our Service or any of our trademarks, logos or other proprietary information without our express written consent.
- We do not grant you the rights to data mine, train on any AI model on our copyrighted material.
- We do not grant you the rights to  download our copyrighted material for any other purpose than the ones related of understanding and using properly our Service
- We do not grant you the right to reverse engineering the parts of our code that is not open-source, or to access/modify the parts of our code that is open source, but in ways that are in violation of applicable the open source license on this code.
- We do not grant you the rights to use the Service other than for its intended purposes, except if we have a written agreement that mentionis an exception to this clause.

If you do not respect the stated rights, we may terminate your use of the Service, and in certain cases, we may ask you for a compensation.


### Security Policy

**Provider** will use commercially reasonable efforts to secure the
**Cloud Service** from unauthorized access, alteration, or use and other
unlawful tampering.

At Yesbot, we prioritize the security and privacy of our customers\' information, data, and intellectual property. Our platform utilizes various encryption techniques and quota verification methods to ensure the confidentiality, integrity, and availability of our services. Below are the key components of our security policy:

-Encryption: Everywhere applicable Yesbot employs industry-standard encryption protocols to safeguard data transmission and storage.
-Quota Verification: Yesbot implements quota verification mechanisms. This ensures that customers adhere to their allocated resources and prevents any single user from monopolizing system resources.
-Data Protection: Yesbot is committed to protecting customer information, data, and intellectual property. We will take all necessary measures within our power to safeguard against unauthorized access, disclosure, or destruction of customer data. However we cannot take responsibility for the damage taken related to the Customer misbehavior or negligence, such as including sensitive data into their API calls that will be recorded by Yesbot. Also Yesbot will not communicate your data to any third-party.
-Compliance with Regulations: Yesbot complies with relevant data protection and privacy regulations, including but not limited to any other applicable laws or standards. We strive to maintain transparency and accountability in our data handling practices.
-Continuous Monitoring and Improvement: Yesbot regularly monitors our systems and infrastructure for potential security vulnerabilities or breaches. We conduct regular security assessments, testing and QA processing to identify and address any weaknesses promptly.
-Incident Response: In the event of a security incident or breach, Yesbot has established procedures for incident response and notification. We will promptly investigate any suspected breaches, take appropriate remedial actions, and notify affected customers as necessary.
-Employee Training and Awareness: Yesbot provides comprehensive security training and awareness programs for all employees. This ensures that our staff are knowledgeable about security best practices and are equipped to handle sensitive information responsibly.
-Third-Party Security: Yesbot evaluates the security practices of third-party vendors and service providers to ensure they meet our standards for data protection and privacy. We only engage with reputable partners who demonstrate a commitment to security.
-Customer Collaboration: Yesbot encourages open communication with customers regarding security concerns or suggestions for improvement. We value customer feedback and are dedicated to addressing any security-related issues promptly.
-Policy Review and Updates: This security policy is subject to periodic review and updates to reflect changes in technology, regulations, or business practices. Customers will be notified of any significant revisions to this policy.

By using Yesbot, customers agree to abide by this security policy and acknowledge that their data will be handled in accordance with these principles. We are dedicated to maintaining the highest standards of security and privacy for our customers.


### Changes to Standard Terms

#### Publicity Rights

##### Modifying Section 14.7 of the Standard Terms

Modifying Section 14.7 of the Standard Terms: **Provider** may identify **Customer** and use **Customer's** logo and trademarks on
**Provider's** website and in marketing materials to identify **Customer** as a user of the **Cloud Service**. **Customer** hereby
grants **Provider** a non-exclusive, royalty-free license to do so in connection with any marketing, promotion, or advertising of **Provider** or the **Cloud Service** during the length of the Agreement. 

#### Other Changes to Standard Terms

##### List of specific changes to the Standard Terms

###### Added sections :
In no particular order, sections **Disclaimers**, **Glossary**, **Yesbot Platform version specificities**, **Offering details**, **Last updated**, **Introduction**, **Changes to these terms**, **Third-Party Services**, **Creating Accounts**, **Customer Content & Conduct**, **Yesbot Platform and Provider Intellectual Property**, **Indemnity**, **Feedback, questions and Comments** have been added to the Standard terms.

###### Section 5.1 of the Standard Terms is to be replaced with the following text :
5.1 Fees and Invoices. All fees are in Canadian Dollars, and are inclusive of taxes if not mentioned otherwise in the bills, contracts or any other document. Except for the prorated refund of prepaid fees allowed with specific termination rights, fees are non-refundable. Provider will send invoices for fees applicable to the Product once per Invoice Period in advance starting on the Subscription Start Date. Invoices for Professional Services may be sent monthly during performance of the Professional Services unless the Cover Page includes a different cadence.

###### Section 14.13 of the Standard Terms is to be replaced with the following text :
14.13 Government Rights. The Cloud Service and Software are deemed “commercial items” or “commercial computer software” according to Canada abd Quebec Law definitions, and the Documentation is “commercial computer software documentation” according to the same definitions sources. Any use, modification, reproduction, release, performance, display, or disclosure of the Product by Canada and Quebec Government will be governed solely by the terms of this Agreement and all other use is prohibited.

Provider and Customer have not changed the Standard Terms except for the details on the Cover Page above. By signing this Cover Page, each party agrees to enter into this Agreement as of the Effective Date.


  ------------------ ------------------------- -- -------------------------
                     **PROVIDER: Alqemia Studio**     **CUSTOMER:**

  **Signature**                                   

  **Print Name**     Jeremie Ratomposon           

  **Title**          Founder & main architect     

  **Notice Address** 6602 41th avenue\            
                     Montreal, Quebec H1T2V1\     
                     Canada                       

  **Date**                                        
  ------------------ ------------------------- -- -------------------------
