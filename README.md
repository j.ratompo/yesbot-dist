# Yesbot


## Name
Yesbot

## What is Yesbot ?
Yesbot is a simple platform that provides various tools to help creating, managing and performing tests on your code.

## Yesbot parts
Yesbot is based on a client-server architecture. The client will serve as an observer for your code. The server will retrieve the data from the client, and hold your tests data. The server will also be able to perform the tsts against your application's code.
Here is the breakdown of Yesbot components
### Client side :
- a library and configuration files, if you want Yesbot to be included in your application context, and provide optional testing features
- a proxy if you want almost no configuration and minimal impact on your application, but at the cost of less features
### Server side :
- the main server, that will hold and analyze the testing data.
- the runner, a component that will perform the tests on your application 

## Screenshots
<img alt="screenshot 1" src="assets/imgs/screenshot1.png" width="50%"/>
<img alt="screenshot 1" src="assets/imgs/screenshot2.png" width="50%"/>

***

## Getting started
If you just want to test Yesbot, we recommend you simply create your account to the Yesbot server, and setup  the Yesbot client proxy.  

### Installation

#### Prerequisites
- You need to have installed the latest version of [nodejs](https://nodejs.org/en/download/current) corresponding to your target environment
- You need to have git installed, or curl

#### Installation
- Download the project code : `git clone https://framagit.org/j.ratompo/yesbot-dist`
- Download the dependencies : `npm install`

## Usage


Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
